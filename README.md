**I've put this project on hold because there is no way of doing the Matrix backend side of this without requiring insecure behaviour from users (typing their credentials into a random website). I'm following the [matrix-spec discussion on transitioning to OAuth2](https://github.com/matrix-org/matrix-spec/issues/636) for this.**



# Predictions

This is an implementation for prediction markets using [Matrix](https://github.com/matrix-org/matrix-js-sdk) as a user provider and [Matrix-CRDT](https://github.com/YousefED/Matrix-CRDT/) for distributed data storage. It is meant for usage by small groups, like an office, and assumes high trust.

## How to use

Click the "Matrix Login" button. Log in with your own matrix account or select "fill crdt1" or "fill crdt2" to prefill login data for some test users. The matrix room alias defines the community for the prediction markets – all users in that room will be able to trade with one another.

Click "reset markets" to reset all orders for the markets.

The prediction market UI is work in progress, but there's a little help box that tries to explain things. See below for an explanation of prediction markets.

At the moment, every new user starts with €0 and can go into debt.

## Prediction Markets

### Overview

A prediction market is a market in which you can bet on the outcome of an event through buying `YES` or `NO` shares. The market price can then give an indication of what the probability of the outcome is.

In this implementation, each `YES` share pays out €1 if the condition obtains at the end of the running period, and each `NO` share pays out €1 if the condition does not obtain at the end of the running period.

If you believe there is an 80% chance of the outcome obtaining, you should be willing to pay up to 80¢ for a `YES` share.

If you believe there is a 30% chance that the outcome will not obtain, you should be willing ot pay up to 30¢ for a `NO` share.

### Trading instead of bookmaking

Unlike with traditional betting, a prediction market generally does not have a broker that sells shares to whoever wants at a predetermined price. Instead, participants place bids (to buy a share at a certain price) and asks (to sell a share at a certain price). I might be assessing the probably of an event at 35%, so I am placing a bid for a `YES` share at 30¢. If there is nobody willing to sell a `YES` share at that price, nothing happens. But if you happen to have a `YES` share that you're willing to sell at that price (perhaps because you think the probability of a `YES` outcome is lower than 30%), then you might place an _ask_ at 30¢. Because my bid matches your ask, we can trade – I pay you ¢30 and you give me a `YES` share, both of us thinking we've got a good deal.

### Market Making

The above example assumes that there are already `YES` or `NO` shares in circulation that can be traded. At the beginning of a market, that is not the case. To kickstart the market, we need a market maker. This market maker uses the fact that one `YES` share together with one `NO` share must be worth exactly €1 at payout, because exactly one of them will pay out at the end of the running time. The market maker is thus always willing to buy 1 `YES` share and 1 `NO` share at the same time for exactly €1, or sell 1 `YES` share and 1 `NO` share together for €1.

If at the beginning of a market I place a bid for a `YES` share at 40¢, and you place a bid for a `NO` share at 60¢, the market maker will match our bids by taking each of our money (for a total of 100¢ or €1) and giving me a `YES` share and you a `NO` share. From the market maker's perspective, 1 `YES` share and 1 `NO` share was sold for €1.

Throughout the running time of the market, the market maker jumps in whenever there is a potential to make a trade happen buy buying or selling the combination of 1 `YES` and 1 `NO` share for €1. Throughout the running time, the market maker will always hold the same number of `YES` and `NO` shares and owe or hold exactly enough money for it all to cancel out at the end of the running time.

### Buying `YES` is selling `NO`

Because `1 YES + 1 NO = €1`, we can formulate an equivalency between buying a `YES` share and selling a `NO` share. Buying a `YES` share at a price p is equivalent to selling a `NO` share at price 100-p. Similarly, selling a `YES` share at a price p is equivalent to buying a `NO` share at price 100-p.

Consider the case where you have these holdings:

- cash: 150¢
- `YES` shares: 3
- `NO` shares: 2

You buy 1 `YES` share for 70¢:

- cash: 80¢
- `YES` shares: 4
- `NO` shares: 2

At payout, you will have €4.80 if the condition obtains and €2.80 if the condition does not obtain.

Now imagine you had started with the same holdings, but sold a `NO` share for 100¢-70¢=30¢

- cash: 180¢
- `YES` shares: 3
- `NO` shares: 1

At payout, you will again have €4.80 if the condition obtains and €2.80 if the condition does not obtain.

Conceptually, you can mentally replace buying `NO` shares with selling `YES` shares and vice versa. In the market maker example above, the market maker allowed the trade to happen between me looking to buy a `YES` share at 40¢ and you looking to buy a `NO` share at 60¢, which equivalent to selling a `YES` share at 100¢-60¢=40¢. Given that I was willing to buy at exactly the price you were looking to sell, it is no surprise a trade could happen.

### Constraints

The prediction market uses the equivalency between buying one side and selling the opposite to enforce some constraints:
Each player may only hold either `YES` shares or `NO` shares. If you hold `YES` shares and place a bid to buy `NO` shares, these bids will be converted to asks for selling some of your `YES` shares at the equivalent price by the market maker.
