import { syncedStore, getYjsValue } from "@syncedstore/core";
import { unref } from "vue";
const prices: Price[] = [10, 20, 30, 40, 50, 60, 70, 80, 90]

const initialOutstandingOrders = {}
prices.forEach(p => (initialOutstandingOrders as OutstandingOrders)[p] = [])
const initialClearedMarket: ClearedMarket = {
  brokerStats: {
    cash: 0,
    yesShares: 0,
    noShares: 0,
    yesBids: 0,
    noBids: 0
  },
  playerStats: {},
  outstandingOrders: initialOutstandingOrders as OutstandingOrders
}

// STORE

const store = syncedStore({ predictions: [] as Prediction[], cashBase: {} as Record<string, number> });

function placeOrCancelOrder(orderOrCancellation: OrderOrCancellation, predictionId: string) {
  const prediction = store.predictions.find(p => p.uuid === predictionId)
  if (!prediction) {
    console.log("could not find prediction with uuid", predictionId)
    store.predictions.forEach(p => console.log(p.uuid))
  } else {
    prediction.timeline.push(orderOrCancellation)
  }
}

function changeCash(userId: string, newCash: number) {
  store.cashBase[userId] = (store.cashBase[userId] ?? 0) + newCash
}

export {
  deriveMarket,
  placeOrCancelOrder,
  addPrediction,
  changeCash,
  prices,
  store
}

// PREDICTIONS
function addPrediction(p: Prediction) {
  store.predictions.push(p)
}

// MARKET FUNCTIONS
function findFirstMatchingOrder(pastOrders: OutstandingOrders, order: Order): (null | { price: Price, index: number }) {
  const priceIndex = prices.indexOf(order.price)
  const searchPriceInOrder = order.isBid ? prices.slice(0, priceIndex+1) : prices.slice(priceIndex, prices.length).reverse()
  for (const price of searchPriceInOrder) { // starting from lowest or highest price depending on Order type
    const resultIndex = pastOrders[price].findIndex(p => {
      return p.isBid !== order.isBid
    })
    if (resultIndex !== -1) return { price, index: resultIndex }
  }
  return null
}

function applyOrder(market: ClearedMarket, order: (Order | Cancellation)) {
  // adding player to player assets
  if (!market.playerStats[order.userId]) {
    market.playerStats[order.userId] = {
      yesBids: 0,
      noBids: 0,
      yesShares: 0,
      noShares: 0,
      cash: 0
    }
  }

  // CANCELLATIONS
  // we try to delete the last order added
  if (order.isCancellation === true) {
    const pastOrders = market.outstandingOrders[order.price]

    
    let i = -1
    for (let j = pastOrders.length-1; j >= 0; j--) {
      const p = pastOrders[j]
      if (p.userId === order.userId && p.isBid === order.isBid) {
        i = j;
        break;
      }
    }

    if (i === -1) {
      // no matching order was found, this usually means there was some conflict
      return market
    }
    market.outstandingOrders[order.price].splice(i, 1)

    // change player stats
    market.playerStats[order.userId][order.isBid ? "yesBids" : "noBids"]--
    return market
  } 

  // ORDER
  
  // first we try to find a matching order to execute a transaction
  const matchingOrderDetails = findFirstMatchingOrder(market.outstandingOrders, order)
  if (matchingOrderDetails === null) {
    // no matching order was found, we add the order to the existing stack of orders
    // the findFirstMatchingOrder function guarantees that the types match here, but
    // typescript doesn't know that

    const outstandingOrders = market.outstandingOrders[order.price]
    outstandingOrders.push(order)
    market.outstandingOrders[order.price] = outstandingOrders

    // change player stats
    market.playerStats[order.userId][order.isBid ? "yesBids" : "noBids"]++
    return market
  } else {
    // there is a transaction we will execute!
    const { price, index } = matchingOrderDetails
    const matchingOrder = market.outstandingOrders[price][index]

    const self  = market.playerStats[order.userId]     
    const other = market.playerStats[matchingOrder.userId]
    
    const moneySpent = (order.isBid ? +1 : -1) * matchingOrder.price // from perspective of self 
    self  .cash -= moneySpent
    other .cash += moneySpent

    const sharesGained = order.isBid ? 1 : -1 // from perspective of self
    self  .yesShares += sharesGained
    other .yesShares -= sharesGained
    
    const broker = market.brokerStats
    for (const assets of [self, other]) {
      if (assets.yesShares === -1 || assets.noShares === -1) {
        // immediately buy one yes and one no share for 100
        assets.yesShares  += 1
        assets.noShares   += 1
        assets.cash -= 100

        broker.yesShares  -= 1
        broker.noShares   -= 1
        broker.cash += 1

      } else if (assets.yesShares >= 1 && assets.noShares >= 1) {
        // immediately sell one yes and no share for 100
        assets.noShares   -= 1
        assets.yesShares  -= 1
        assets.cash += 100

        broker.yesShares  += 1
        broker.noShares   += 1
        broker.cash -= 1
      }
    }

    other[order.isBid ? "noBids" : "yesBids"]--

    market.playerStats[order.userId] = self
    market.playerStats[matchingOrder.userId] = other
    market.brokerStats = broker

    // remove matched order
    market.outstandingOrders[price].splice(index, 1)
    return market
  }
}

function deriveMarket(timeline: Timeline) {
  let array = timeline.map(o => o) // somehow needing this to make timeline an array again?
  const market = array.reduce(applyOrder, JSON.parse(JSON.stringify(initialClearedMarket)))
  return market
}

// SETUP
function setup() {
  const orderA: Order = {
    userId: "simon",
    price: 20,
    isCancellation: false,
    isBid: true,
    uuid: "someuuid"
  }

  const orderB: Order = {
    userId: "simon",
    price: 30,
    isCancellation: false,
    isBid: true,
    uuid: "someuuid"
  }

  const orderC: Order = {
    userId: "someone else",
    price: 50,
    isCancellation: false,
    isBid: false,
    uuid: "someuuid"
  }

  // placeOrCancelOrder(orderA)
  // placeOrCancelOrder(orderB)
  // placeOrCancelOrder(orderC)
  // placeOrCancelOrder(orderA)
  // placeOrCancelOrder(orderC)
}
setup()