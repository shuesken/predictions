type Price = 10 | 20 | 30 | 40 | 50 | 60 | 70 | 80 | 90
interface OrderOrCancellation {
  uuid: string,
  userId: string,
  price: Price,
  isCancellation: boolean,
  isBid: boolean
}

type Timeline = OrderOrCancellation[]

interface Stats {
  yesShares: number
  noShares: number
  cash: number
  yesBids: number
  noBids: number
}

type Cancellation = OrderOrCancellation & { isCancellation: true }
type Order = OrderOrCancellation & { isCancellation: false }

// type BidOrder = Order & { isBid: true }
// type AskOrder = Order & { isBid: false }
type OutstandingOrders = Record<Price, Order[]> // could be reinfed to (AskOrder[] | BidOrder[]), but that's been a hassle
  
interface ClearedMarket {
  outstandingOrders: OutstandingOrders
  playerStats: Record<string, Stats>
  brokerStats: Stats
}

type Prediction = {
  uuid: string,
  timeline: Timeline,
  clearedMarket?: ClearedMarket
  end: string, // double check that this works and will be fine with string output
  description: string
  title: string,
  condition: string,
}